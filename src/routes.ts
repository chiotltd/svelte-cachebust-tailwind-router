// routes.ts - define constants for each of the routes (to make this simpler to change in the future)
export const dashboardPath = '/dashboard';
export const teamPath = '/team';
export const projectsPath = '/projects';
export const calendarPath = '/calendar';
export const reportsPath = '/reports';