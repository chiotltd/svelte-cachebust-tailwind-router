# Svelte - Template with extras

This folder contains a template based upon the standard [sveltejs/template](https://github.com/sveltejs/template) (as at 2021-01-31; commit e6e47d658ca61bf7bcb82d7424e5c2a3a21814ce) with the following additions:

* Cache busting in the production build (filenames have a hash added)
* [tailwindcss](https://tailwindcss.com/) - integrated into the build process
* [typescript](https://www.typescriptlang.org/) - typesafe coding.
* [tinro](https://github.com/AlexxNB/tinro) - A simple router.

This readme steps through the process followed to create the template (and will simplify the process of updating it when new versions of the base template are released). Please see the readme in the source template if you are after general info.

If you wish to use this template you can grab it as-is from the repository or follow the below instructions to create your own version. Note that changes to the base template may mean that the below instructions do not work exactly as written!

*Note: This was created as I investigated svelte for the first time so there are probably better alternatives!*

## Install Base template

This is covered in the [svelte blog](https://svelte.dev/blog/the-easiest-way-to-get-started) but the basic commands are:

```bash
npx degit sveltejs/template my-svelte-project
cd my-svelte-project
npm install
npm run dev
```
This will start the template app; you can view it on http://localhost:5000.

`npm run build` will to create a 'production-ready' version of the app (excludes runtime checks/debugging code and uses [terser](https://github.com/terser/terser) to make it smaller).

*Note: I created this repository and then copied the template into it with `npx degit sveltejs/template .\svelte-cachebust-material-router --force`*

*Note2: `.gitignore` was updated to avoid committing unnecessary files .*

## Install `rollup-plugin-styles`

The default template uses `rollup-plugin-css-only` to combine and output the projects css files. This works OK in a very basic application but unfortunately it does not support the rollup [`assetFileNames`](https://rollupjs.org/guide/en/#outputassetfilenames) option (this is why the default template passes the name to the plugin `css({ output: 'bundle.css' })`).  Using the `assetFileNames` option (with plugins that support it) enables us to control the names of the files that rollup will output in one place and really simplifies things like cache busting.

[`rollup-plugin-styles`](https://anidetrix.github.io/rollup-plugin-styles/index.html) is a plugin that includes a lot of css based functionality (e.g. transpiling scss to css) in addition to extraction that supports the use of the  [`assetFileNames`](https://rollupjs.org/guide/en/#outputassetfilenames) option. This template will only be using the extraction functionality so there may be simpler alternatives but as this works I’ll stick with it. To install run:

````bash
npm install -D rollup-plugin-styles
````

Next we need to edit `/rollup.config.js` so that this will be used. Firstly replace `import css from 'rollup-plugin-css-only';` with `import styles from 'rollup-plugin-styles';`. Next replace

```javascript
// we'll extract any component CSS out into
// a separate file - better for performance
css({ output: 'bundle.css' }),
​```
```

with:

```
// rollup-plugin-styles - currently only used to extract CSS for better performance
styles({
    mode: "extract",
    minimize: production,
}),
```

We also need to update the `output` section so it uses the new directives to define where the output should be written. Replace:

```
output: {
    sourcemap: true,
    format: 'iife',
    name: 'app',
    file: 'public/build/bundle.js'
},
```

with

```
output: {
    sourcemap: true,
    format: 'iife',
    name: 'app',
    dir: 'public',
    entryFileNames: `build/[name].js`,
    chunkFileNames: `build/[name].js`,
    assetFileNames: `build/[name].[ext]`,
},
```

Finally we need to update `public/index.html` so that it uses the new names for the bundle files; replace:

```html
<link rel='stylesheet' href='/build/bundle.css'>
<script defer src='/build/bundle.js'></script>
```

with:

```html
<link rel='stylesheet' href='/build/main.css'>
<script defer src='/build/main.js'></script>
```

## Cache busting for production build

*Note: This step can be skipped if it's not needed for your project.  If you are using a CDN (or webserver configured to add hashes to urls) then there may be no need to implement this yourself.*

`npm run build` generates files (in `/public/build`) that you could deploy to production but there may be issues when applying upgrades. This is because the app is in ```build/bundle.css``` and ```build/bundle.js``` and the browser may cache these (this depends upon webserver configuration but ideally you want these files to be cached except when an update has been deployed). A fairly standard way to work around this is to add a hash to the filenames of the application components and update the `index.html` to load the files with the current hashes (this means that when the file changes its name also changes meaning the browser will request it from the server). [This article](https://medium.com/@codebyamir/a-web-developers-guide-to-browser-caching-cc41f3b73e7c) provides a good overview.

*Note*: There are a few open issues ([sveltejs #39](https://github.com/sveltejs/template/issues/39) [sveltejs #61](https://github.com/sveltejs/rollup-plugin-svelte/issues/61), [rollup-plugin-css-only #25](https://github.com/thgh/rollup-plugin-css-only/issues/25)) and the rapid rate of change with Svelte/Rollup means documentation/tutorials are often out of date. While there were some solutions available ([svelte-rollup-template](https://github.com/metonym/svelte-rollup-template),
[obs-web](https://github.com/Niek/obs-web/blob/master/rollup.config.js)) these did not make use of recently introduced functionality (e.g. `assetFileNames` in rollup) and I encountered some issues when testing them. As a result I have created my own solution (drawing on a range of resources) in an attempt to come up with something that works for me.

My aims are:

* Make the process understandable (avoid adding too many dependencies that make it difficult to follow how this works).
* Have as little impact on the development process as possible (ideally `npm run dev` should work exactly as it does in the base template).
* Efficienct use of cache - files should be cached unless a change has been made (eliminates options that add the build date to the filename).  
* Keep changes to `rollup.config.js` to a minimum to make it easier to update to new versions of the template.
* Use `output.entryFileNames` and `output.chunkFileNames` options as suggested by [Rich Harris](https://twitter.com/rich_harris/status/1079991930623283200)
* Deploy production builds to a separate folder (`/dist`) to simplify deployment and ensure development builds do not end up in production!

Hopefully the process can be simplified in the future.

### Copy in new files

The `/build-util` folder needs to be created and a few utility functions copied in (grab the folder from this repository). These javascript files will be used in `rollup.config.js` (putting these in a separate file minimises the changes needed there). When rebuilding from scratch it is probably simplest to copy these files from this repo. The files are:

* index-template.js - Code to generate an `index.html` that references the bundled files using names that include the hash.
* generate-dist.js - Code to copy `/public/` to `/dist/` and make the changes needed to get everything running.

The files all contain some comments so hopefully it is not too difficult to see what they are doing (and fix any issues!).

### Install required modules

Run:
```bash
npm install -D posthtml posthtml-hash posthtml-urls fs-extra
```

These modules are:

* `posthtml` - a tool for transforming HTML/XML with JS plugins ([github](https://github.com/posthtml/posthtml)).
* `posthtml-hash` - plugin for the above that adds hashes to filenames (by default it replaces the `hash` within a filename something like `bundle.[hash].js`) 
* `posthtml-urls` - plugin for posthtml that simplifies finding/changing urls within a html file.
* `fs-extra` - file sytem functions used when generating `dist`

### Update `rollup.config.js`

Because most of the work is done in `build-util` the changes needed in `rollup.config.js` are relatively simple:

#### includes

Under the existing includes add:

```javascript
import {indexTemplate} from './build-util/index-template'
import {generateDist} from './build-util/generate-dist'
```

#### Add constants

Add the following after the imports (after the existing `const production`):

```javascript
const build_dir = "public";
const prod_dir = "dist";
```

### Reconfigure rollup outputs

We need to update the output filenames so that these will include the hash (in production only). Update these (under `default.output`) to be:

```javascript
entryFileNames: `build/[name]${production ? '.[hash]' : ''}.js`,
chunkFileNames: `build/[name].chunk${production ? '.[hash]' : ''}.js`,
assetFileNames: `build/[name]${production ? '.[hash]' : ''}.[ext]`,
```

This change tells rollup to add hashes to the files it generates for production builds.

#### Generate `index.html` and `dist` folder

Under ```production && terser()``` (add a `,` to the end of this line) add:

```javascript
// For production builds index.html needs to be rewritten with filenames containing th hashes
// note: this only covers the bundle files (global.css is fixed later)
production && indexTemplate(),

// Production builds will be moved to the dist folder and a hash added to global.css
production && generateDist(build_dir, prod_dir)
```

### Test everything still works

Run `npm run dev` and verify that everything still works.

Run `npm run build` and verify that a `dist` folder is created and populated. 

To test that the production files can be served OK lets add a new run action in `package.json`. Under `"start": "sirv public"` add `"startprod": "sirv dist"` then run `npm run startprod`; you should see the hello world page.

*Note: In production you may see that a file `inter.css` is being requested but a 404 returned. This is because `index-template.js` in the repository has been changed to include a font; either ignore this error or remove the include.*

## typescript

I plan on using typescript (rather than javascript) so lets convert the template (better to do this before too many other changes are made). To do this run:

```bash
node scripts/setupTypeScript.js
npm install
```

You will need to undo the changes this makes to `rollup.config.js` because we have already added the needed tools.

Test that everything still works with `npn run dev` (and also `npm run build`).

You may get an error regarding source maps when running `npm run build`; to fix this edit `rollup.config.js` and change:

```
typescript({
   sourceMap: !production,
   inlineSources: !production
})
```

to:

```
typescript({
   sourceMap: true,
   inlineSources: !production
})
```

## Install Tailwindcss

The web interface will be designed using [TailwindCSS](tailwindcss.com)  with many components coming from [TailwindUI](https://tailwindui.com/). While Tailwind can be used without any extra tooling this leads to very large CSS files; so we install the relevant tooling here.

### Fonts

TailwindUI examples use the [Inter](https://rsms.me/inter) font (see [https://tailwindui.com/documentation](https://tailwindui.com/documentation)) so we will too. In order to avoid the need for an internet connection the font will be self hosted. We will install this from it’s source (this means the files should be checked into git)

* Download the font from `https://rsms.me/inter/`
* Copy the following into `/public/assets/inter` 
  * Inter Web/inter.css
  * Inter Web/Inter-roman.var.woff2
  * Inter Web/Inter-italic.var.woff2
* Edit `/public/font/`inter.css  and remove everything except the variable font section.
* Edit `/public/index.html` and `build-util.index-template.js` to utilise the new font (`<link rel="stylesheet" href="/assets/inter/inter.css">`)
* To test add `font-family: "Inter var";` into the `main` `<style>` in `/src/App.svelte`.

Test the app and confirm (using inspect element) that the new font is in use.

*Note: This font will be set as the default in the tailwind configuration file.*

### Tailwind

There are number of different ways that tailwind can be installed. There is a good summary [here](https://www.apostrof.co/blog/getting-tailwind-css-to-integrate-nicely-with-svelte-sapper/) but the most up-to-date process seems to be [this one](https://www.swyx.io/svelte_tailwind_setup/) (which I will follow). Firstly install the needed tools (see the [tailwind docs](https://tailwindcss.com/docs/using-with-preprocessors) for info on these):

```bash
npm install -D svelte-preprocess tailwindcss autoprefixer postcss-nesting postcss
```

*Note:  Some of the modules will already be installed but have included them here in case previous steps have been skipped. When using TailwindUI you may need to add `@tailwindcss/typography`, `@tailwindcss/aspect-ratio`, and/or `@tailwindcss/forms` plugins*

Run `npx tailwindcss init` - this will create a minimal `/tailwind.config.js` which should now be changed to: (note that this change should not remove any sections so it’s worth doing it bit by bit in case the default config has changed since this was written)

Now create `/tailwind.config.js` as follows:

```javascript
// tailwind.config.js
const { tailwindExtractor } = require("tailwindcss/lib/lib/purgeUnusedStyles");
const defaultTheme = require('tailwindcss/defaultTheme')

const production = !process.env.ROLLUP_WATCH;

module.exports = {
  purge: {
    enabled: production, // disable purge in dev,
    // Note: see https://tailwindcss.com/docs/optimizing-for-production#removing-all-unused-styles for possible
    // enhancements (not recommended)
    content: [
      "./src/**/*.svelte",
      "./public/index.html"
    ],
    options: {
      // As per https://github.com/tailwindlabs/tailwindcss/discussions/1731 - prevents purge from removing
      // items using the svelte class: directive
      defaultExtractor: (content) => [
        ...tailwindExtractor(content),
        // Match Svelte class: directives (https://github.com/tailwindlabs/tailwindcss/discussions/1731)
        ...[...content.matchAll(/(?:class:)*([\w\d-/:%.]+)/gm)].map(([_match, group, ..._rest]) => group),
      ],
      keyframes: true,
    }
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
```

Next we need to update `rollup.config.js`; firstly add an import (if not already there!):

```js
import sveltePreprocess from "svelte-preprocess";
```

then add/update the `preprocess` section under the `svelte` plugin to:

```
preprocess: sveltePreprocess({
				// https://github.com/kaisermann/svelte-preprocess/#user-content-options
				sourceMap: !production,
				postcss: {
					plugins: [require("tailwindcss"), require("autoprefixer"), require("postcss-nesting")]
				}
			}),
```

As `tailwind` manages styles lets clear those currently setup; delete the contents of `public/global.css`.

Finally we need to add the tailwind includes and update the template to demonstrate that everything is working (using some `tailwindui` components). I decided to split the app into components at this stage because it will simplify future changes.

Create `/src/Tailwindcss.svelte` (note: separate file used because after any changes rebuild will be slow) as:

```html
<style global>
  @tailwind base;
  @tailwind components;
  @tailwind utilities;
</style>
```



Also create a new file `/src/Dashboard.svelte`:

```html
<script lang="ts">
    export let name: string;
</script>

<div class="flex flex-col">
    <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div
                    class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg"
            >
                <table class="min-w-full divide-y divide-gray-200">
                    <thead class="bg-gray-50">
                    <tr>
                        <th
                                scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                            Name
                        </th>
                        <th
                                scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                            Title
                        </th>
                        <th
                                scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                            Status
                        </th>
                        <th
                                scope="col"
                                class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                            Role
                        </th>
                        <th scope="col" class="relative px-6 py-3">
                            <span class="sr-only">Edit</span>
                        </th>
                    </tr>
                    </thead>
                    <tbody class="bg-white divide-y divide-gray-200">
                    <tr class="hover:bg-gray-200">
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="ml-4">
                                <div
                                        class="text-sm font-medium text-gray-900"
                                >
                                    Jane Cooper
                                </div>
                                <div class="text-sm text-gray-500">
                                    jane.cooper@example.com
                                </div>
                            </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="text-sm text-gray-900">
                                Regional Paradigm Technician
                            </div>
                            <div class="text-sm text-gray-500">
                                Optimization
                            </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                                <span
                                        class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                                >
                                    Active
                                </span>
                        </td>
                        <td
                                class="px-6 py-4 whitespace-nowrap text-sm text-gray-500"
                        >
                            Admin
                        </td>
                        <td
                                class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium"
                        >
                            <a
                                    href="#/"
                                    class="text-indigo-600 hover:text-indigo-900"
                            >Edit</a
                            >
                        </td>
                    </tr>
                    <tr class="hover:bg-gray-200">
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="ml-4">
                                <div
                                        class="text-sm font-medium text-gray-900"
                                >
                                    {name}
                                </div>
                                <div class="text-sm text-gray-500">
                                    jane.cooper@example.com
                                </div>
                            </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                            <div class="text-sm text-gray-900">
                                Regional Paradigm Technician
                            </div>
                            <div class="text-sm text-gray-500">
                                Optimization
                            </div>
                        </td>
                        <td class="px-6 py-4 whitespace-nowrap">
                                <span
                                        class="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
                                >
                                    Active
                                </span>
                        </td>
                        <td
                                class="px-6 py-4 whitespace-nowrap text-sm text-gray-500"
                        >
                            Admin
                        </td>
                        <td
                                class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium"
                        >
                            <a
                                    href="#/"
                                    class="text-indigo-600 hover:text-indigo-900"
                            >Edit</a
                            >
                        </td>
                    </tr>

                    <!-- More items... -->
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
```

another new file called `/src/Navbar.svelte` 

```html
<script lang="ts">
    import {scale} from 'svelte/transition';
    import {cubicIn, cubicOut} from 'svelte/easing';

    let mobileMenuOpen: boolean = false;
    let notificationsOpen: boolean = false;
    let userMenuOpen: boolean = false;
</script>

<style>
    /* Router link styling changes depending upon whether the link is active or not */
    :global(.active).routerLink {
        @apply bg-gray-900 text-white
    }

    .routerLink:not(.active) {
        @apply text-gray-300 hover:bg-gray-700 hover:text-white
    }
</style>


<!--Note: The below is from Tailwind UI free sample components (https://tailwindui.com/preview) -->
<nav class="bg-gray-800">
    <div class="mx-auto px-4 sm:px-6 lg:px-8">
        <div class="flex items-center justify-between h-16">
            <div class="flex items-center">
                <div class="flex-shrink-0">
                    <img
                            class="h-8 w-8"
                            src="https://tailwindui.com/img/logos/workflow-mark-indigo-500.svg"
                            alt="Workflow"
                    />
                </div>
                <div class="hidden md:block">
                    <div class="ml-10 flex items-baseline space-x-4">
                        <!-- Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" -->
                        <a href="#/"
                           class="routerLink px-3 py-2 rounded-md text-sm font-medium">Dashboard</a>
                        <a href="#/"
                           class="routerLink px-3 py-2 rounded-md text-sm font-medium">Team</a>
                        <a href="#/"
                           class="routerLink px-3 py-2 rounded-md text-sm font-medium">Projects</a>
                        <a href="#/"
                           class="routerLink px-3 py-2 rounded-md text-sm font-medium">Calendar</a>
                        <a href="#/"
                           class="routerLink px-3 py-2 rounded-md text-sm font-medium">Reports</a>
                    </div>
                </div>
            </div>
            <div class="hidden md:block">
                <div class="ml-4 flex items-center md:ml-6">
                    <button
                            on:click={() => {notificationsOpen = !notificationsOpen;}}
                            class="bg-gray-800 p-1 rounded-full text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                    >
                        <span class="sr-only">View notifications</span>
                        <!-- Heroicon name: bell -->
                        <svg
                                class="h-6 w-6"
                                xmlns="http://www.w3.org/2000/svg"
                                fill="none"
                                viewBox="0 0 24 24"
                                stroke="currentColor"
                                aria-hidden="true"
                        >
                            <path
                                    stroke-linecap="round"
                                    stroke-linejoin="round"
                                    stroke-width="2"
                                    d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
                            />
                        </svg>
                    </button>

                    <!-- Profile dropdown -->
                    <div class="ml-3 relative">
                        <div>
                            <button
                                    on:click={() => {userMenuOpen = !userMenuOpen; console.log(userMenuOpen);}}
                                    class="max-w-xs bg-gray-800 rounded-full flex items-center text-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                                    id="user-menu"
                                    aria-haspopup="false"
                            >
                                <span class="sr-only">Open user menu</span>
                                <img
                                        class="h-8 w-8 rounded-full"
                                        src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                                        alt=""
                                />
                            </button>
                        </div>
                        <!--
                    Profile dropdown panel, show/hide based on dropdown state.

                    Entering: "transition ease-out duration-100"
                      From: "transform opacity-0 scale-95"
                      To: "transform opacity-100 scale-100"
                    Leaving: "transition ease-in duration-75"
                      From: "transform opacity-100 scale-100"
                      To: "transform opacity-0 scale-95"
                  -->
                        {#if userMenuOpen}
                            <div
                                    in:scale={{ duration: 100, start: 0.95, easing: cubicOut }}
                                    out:scale={{ duration: 75, start: 0.95, easing: cubicIn }}
                                    class="origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5"
                                    role="menu"
                                    aria-orientation="vertical"
                                    aria-labelledby="user-menu"
                            >
                                <a
                                        href="#/}"
                                        class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                                        role="menuitem">Your Profile</a
                                >

                                <a
                                        href="#/"
                                        class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                                        role="menuitem">Settings</a
                                >

                                <a
                                        href="#/"
                                        class="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100"
                                        role="menuitem">Sign out</a
                                >
                            </div>
                        {/if}
                    </div>
                </div>
            </div>
            <div class="-mr-2 flex md:hidden">
                <!-- Mobile menu button -->
                <button
                        on:click={() => {console.log(mobileMenuOpen); mobileMenuOpen = !mobileMenuOpen;}}
                        class="bg-gray-800 inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                >
                    <span class="sr-only">Open main menu</span>
                    <!--
                  Heroicon name: menu

                  Menu open: "hidden", Menu closed: "block"
                -->
                    <svg
                            class:block={!mobileMenuOpen} class:hidden={mobileMenuOpen}
                            class="h-6 w-6"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            aria-hidden="true"
                    >
                        <path
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M4 6h16M4 12h16M4 18h16"
                        />
                    </svg>
                    <!--
                  Heroicon name: x

                  Menu open: "block", Menu closed: "hidden"
                -->
                    <svg
                            class:block={mobileMenuOpen} class:hidden={!mobileMenuOpen}
                            class="h-6 w-6"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            aria-hidden="true"
                    >
                        <path
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M6 18L18 6M6 6l12 12"
                        />
                    </svg>
                </button>
            </div>
        </div>
    </div>

    <!--
          Mobile menu, toggle classes based on menu state.

          Open: "block", closed: "hidden"
        -->
    <div class:block={mobileMenuOpen} class:hidden={!mobileMenuOpen} class="md:hidden">
        <div class="px-2 pt-2 pb-3 space-y-1 sm:px-3">
            <!-- Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" -->
            <a href="#/"
               class="routerLink px-3 py-2 rounded-md text-sm font-medium">Dashboard</a>
            <a href="#/"
               class="routerLink px-3 py-2 rounded-md text-sm font-medium">Team</a>
            <a href="#/"
               class="routerLink px-3 py-2 rounded-md text-sm font-medium">Projects</a>
            <a href="#/"
               class="routerLink px-3 py-2 rounded-md text-sm font-medium">Calendar</a>
            <a href="#/"
               class="routerLink px-3 py-2 rounded-md text-sm font-medium">Reports</a>
        </div>
        <div class="pt-4 pb-3 border-t border-gray-700">
            <div class="flex items-center px-5">
                <div class="flex-shrink-0">
                    <img
                            class="h-10 w-10 rounded-full"
                            src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80"
                            alt=""
                    />
                </div>
                <div class="ml-3">
                    <div class="text-base font-medium leading-none text-white">
                        Tom Cook
                    </div>
                    <div class="text-sm font-medium leading-none text-gray-400">
                        tom@example.com
                    </div>
                </div>
                <button
                        class="ml-auto bg-gray-800 flex-shrink-0 p-1 rounded-full text-gray-400 hover:text-white focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-offset-gray-800 focus:ring-white"
                >
                    <span class="sr-only">View notifications</span>
                    <!-- Heroicon name: bell -->
                    <svg
                            class="h-6 w-6"
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke="currentColor"
                            aria-hidden="true"
                    >
                        <path
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9"
                        />
                    </svg>
                </button>
            </div>
            <div class="mt-3 px-2 space-y-1">
                <a
                        href="#/"
                        class="block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700"
                >Your Profile</a
                >

                <a
                        href="#/"
                        class="block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700"
                >Settings</a
                >

                <a
                        href="#/"
                        class="block px-3 py-2 rounded-md text-base font-medium text-gray-400 hover:text-white hover:bg-gray-700"
                >Sign out</a
                >
            </div>
        </div>
    </div>
</nav>
```



and replace `App.svelte` with:

```html
<script lang="ts">
    import Tailwindcss from "./Tailwindcss.svelte";

    import NavBar from './Navbar.svelte';
    import UserGrid from './Usergrid.svelte';

    export let name: string;
</script>

<!--Note: The below is from Tailwind UI free sample components (https://tailwindui.com/preview) -->
<NavBar/>
<UserGrid name='{name}'/>
```

Notes: 

* When running `npm run dev` the initial build will be fairly slow but after that things should run more quickly.
* There are unused styles in `/src/Navbar.svelte` - this will cause a warning to be output (this can be ignored; the styles will be used in the next section).

* Note2: GoLand did not work well with this config; VS code is much easier to work with



## tinro - routing

Svelte does not have a standard router. There is one built into [snapper](https://sapper.svelte.dev/) and there are a range of others ()[see svelte-community](https://svelte-community.netlify.app/code?tag=routers)).

As snapper is to be [replaced by SvelteKit](https://svelte.dev/blog/whats-the-deal-with-sveltekit) I did not want to put much effort into learning that. [Routify](https://routify.dev) is very popular but I felt it was over complicated for my needs (it has a cli and application that performs code generation). As I do not need SSR and just wanted something very simple. I tried [tinro](https://github.com/AlexxNB/tinro) based upon comments in [Svelte Radio](https://podcasts.google.com/feed/aHR0cHM6Ly9mZWVkcy50cmFuc2lzdG9yLmZtL3N2ZWx0ZS1yYWRpbw) (29th Nov 2020) (sounded very simple and easy to use).

To install tinro with `npm install -D tinro`

### Basic Routing

The template really should have some routes if we are to demonstrate tinro. As the demo page already has some options (Dashboard, Team, Pojects, Calendar, World - world is to one that demonstrates the use of parameters) we will create routes for each of these (renaming world back to Reports)

Create the following files - each should just contain the file name (or whatever you want to display in that route):

* `/src/Team.svelte`
* `/src/Pojects.svelte`
* `/src/Calendar.svelte`
* `/src/Reports.svelte`

Rather than hardcoding routes I feel its best to specify the paths in a seperate file so that they can be more easily changed if needed (each of these will be used in multiple places). Create `/src/routes.ts` as:

```typescript
// routes.ts - define constants for each of the routes (to make this simpler to change in the future)
export const dashboardPath = '/dashboard';
export const teamPath = '/team';
export const projectsPath = '/projects';
export const calendarPath = '/calendar';
export const reportsPath = '/reports';
```

We will create the router component in `/src/Route.svelte`:

```html
<script lang="ts">
    import {Route} from 'tinro';
    import Dashboard from './Dashboard.svelte';
    import Team from './Team.svelte';
    import Projects from './Projects.svelte';
    import Calendar from './Calendar.svelte';
    import Reports from './Reports.svelte';

    import * as routes from './routes.ts';

    export let name:string;
</script>

<Route>
    <Route path={routes.dashboardPath}><Dashboard name={name}/></Route>
    <Route path={routes.teamPath}><Team/></Route>
    <Route path={routes.projectsPath}><Projects/></Route>
    <Route path={routes.calendarPath}><Calendar/></Route>
    <Route path={routes.reportsPath}><Reports/></Route>
    <Route fallback redirect="{routes.dashboardPath}"/>
</Route>
```

Update `/src/App.svelte` to make use of this:

```html
<script lang="ts">
    import Tailwindcss from "./Tailwindcss.svelte";

    import NavBar from './Navbar.svelte';
    import Route from './Route.svelte';
    
    let name:string;
</script>

<NavBar/>
<Route name='{name}'/>  <!--route outlet-->
```

Now we will `Navbar.svelte` so that clicking on a link will direct users to the appropriate page (and style the link). There are two identical sections (desktop/mobile) that need to be updated from:

```html
<a href="#/"
    class="routerLink px-3 py-2 rounded-md text-sm font-medium">Dashboard</a>
<a href="#/"
    class="routerLink px-3 py-2 rounded-md text-sm font-medium">Team</a>
<a href="#/"
    class="routerLink px-3 py-2 rounded-md text-sm font-medium">Projects</a>
<a href="#/"
    class="routerLink px-3 py-2 rounded-md text-sm font-medium">Calendar</a>
<a href="#/"
    class="routerLink px-3 py-2 rounded-md text-sm font-medium">Reports</a>
```

to:

````html
<a href="{routes.dashboardPath}" use:active
    class="routerLink px-3 py-2 rounded-md text-sm font-medium">Dashboard</a>
<a href="{routes.teamPath}" use:active
   class="routerLink px-3 py-2 rounded-md text-sm font-medium">Team</a>
<a href="{routes.projectsPath}" use:active
    class="routerLink px-3 py-2 rounded-md text-sm font-medium">Projects</a>
<a href="{routes.calendarPath}" use:active
    class="routerLink px-3 py-2 rounded-md text-sm font-medium">Calendar</a>
<a href="{routes.reportsPath}" use:active
    class="routerLink px-3 py-2 rounded-md text-sm font-medium">Reports</a>
````

You will also need to add two imports:

```javascript
import {active} from 'tinro';
import * as routes from './routes.ts';
```

### Web server settings

The above works fine if you click Home/About. However there is an issue; if we wanted to go straight to the About page the logical thing to do would be to enter `http://127.0.0.1:5000/about` into the address bar but this will not work. The reason for this is that the routing is only on the client side; the web server will return `404 Not Found` for the about page. Fortunately there is a simple solution to this; open `package.json` and add the argument `--single` to all calls to `serv` i.e.

 * `"start": "sirv public",` --> `"start": "sirv public --single",`
 * `"startprod": "sirv dist",` --> `"startprod": "sirv dist --single",`

### Test

Restart the server and the routing should work. `--single` instructs the server to return the `index.html` file for any request where the requested file did not exist.

### Lazy loading

*Note: This has not been applied to the code in the repo (or tested with this template) because it does not work in test and has no real benefit at this stage*

To check that the production code generation will work with additional chunks I implemented lazy loading. This was fairly easy; the first step being to add `/src/lazy.svelte` (copied directly from [here](https://github.com/AlexxNB/tinro#lazy-loading-components))

```html
<!-- Lazy.svelte-->
<script>
    export let component;
</script>

{#await component.then ? component : component()}
    Loading component...
{:then Cmp}
    <svelte:component this={Cmp.default} />
{/await}
```

The next step was edit `Route.svelte` replacing `import Reports from './Reports.svelte';` with `import Lazy from './lazy.svelte';` and  replace:

```html
<Route path={routes.reportsPath}><Reports/></Route>
```

with:

```html
<Route path={routes.reportsPath}>Lazy component={()=>import('./Reports.svelte')}/></Route>
```

Finally in `rollup.config.js` replace `format: 'iife',` with `format: 'es',`.

`npm run build` then `npm run serveprod` enables you to test this. Running in production was fine but got `Uncaught SyntaxError: export declarations may only appear at top level of a module` in test (and have not bothered to investigate).