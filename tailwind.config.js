// tailwind.config.js
const { tailwindExtractor } = require("tailwindcss/lib/lib/purgeUnusedStyles");
const defaultTheme = require('tailwindcss/defaultTheme')

const production = !process.env.ROLLUP_WATCH;

module.exports = {
  purge: {
    enabled: production, // disable purge in dev,
    // Note: see https://tailwindcss.com/docs/optimizing-for-production#removing-all-unused-styles for possible
    // enhancements (not recommended)
    content: [
      "./src/**/*.svelte",
      "./public/index.html"
    ],
    options: {
      // As per https://github.com/tailwindlabs/tailwindcss/discussions/1731 - prevents purge from removing
      // items using the svelte class: directive
      defaultExtractor: (content) => [
        ...tailwindExtractor(content),
        // Match Svelte class: directives (https://github.com/tailwindlabs/tailwindcss/discussions/1731)
        ...[...content.matchAll(/(?:class:)*([\w\d-/:%.]+)/gm)].map(([_match, group, ..._rest]) => group),
      ],
      keyframes: true,
    }
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        sans: ['Inter var', ...defaultTheme.fontFamily.sans],
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}