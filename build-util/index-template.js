/* indexTemplate.js

Based on @rollup/plugin-html (simpler just to replicate the functionality of the plugin as generation of the
is most of the code...
the aim is to match the standard index.html as closely as possible.
*/
import path from 'path';

const supportedFormats = ['es', 'esm', 'iife', 'umd'];
const homePageTemplate = (metas, title, links, scripts) => `<!DOCTYPE html>
<html lang="en">
<head>
${metas}
   
    <title>${title}</title>
   
    <link rel='icon' type='image/png' href='/favicon.png'>
    <link rel="stylesheet" href='/assets/inter/inter.css'>
    <link rel='stylesheet' href='/global.css'>
    ${links}
    
    ${scripts}    
</head>

<body>
</body>
</html>`;

const defaults = {
    attributes: {
        link: null,
        attributes: { html: {lang: 'en'} },
        script: null
    },
    fileName: 'build/index.html', // Write to build folder so we dont overwrite the dev one
    meta: [{ charset: 'utf-8'}, {name: 'viewport', content:'width=device-width,initial-scale=1'}],
    publicPath: '/',
    title: 'Svelte app'
};

// makeHtmlAttributes - combine attributes (copied from @rollup/plugin-html with small change to permit single quotes)
const makeHtmlAttributes = (attributes, quote=`'`) => {
    if (!attributes) {
        return '';
    }

    const keys = Object.keys(attributes);
    // eslint-disable-next-line no-param-reassign
    return keys.reduce((result, key) => (result += ` ${key}=${quote}${attributes[key]}${quote}`), '');
};

// getFiles retrieves the files from the bundle into a map by extension (copied from @rollup/plugin-html)
const getFiles = (bundle) => {
    const files = Object.values(bundle).filter((file) => file.type === 'chunk' ||
        (typeof file.type === 'string' ? file.type === 'asset' : file.isAsset));
    const result = {};
    for (const file of files) {
        const { fileName } = file;
        const extension = path.extname(fileName).substring(1);
        result[extension] = (result[extension] || []).concat(file);
    }
    return result;
};

// indexTemplate generates the index.html file
export function indexTemplate(opts = {}) {
    const { attributes, fileName, meta, publicPath, template, title } = Object.assign({}, defaults, opts);
    return {
        name: 'indexTemplate',
        async generateBundle(output, bundle) {
            if (!supportedFormats.includes(output.format) && !opts.template) {
                this.warn(`index-template: The output format '${output.format}' is not directly supported. A custom \`template\` is probably required. Supported formats include: ${supportedFormats.join(', ')}`);
            }
            if (output.format === 'es') {
                attributes.script = Object.assign({}, attributes.script, {
                    type: 'module'
                });
            }
            const files = getFiles(bundle);
            const scripts = (files.js || [])
                .map(({ fileName }) => {
                    const attrs = makeHtmlAttributes(attributes.script);
                    return `<script defer src='${publicPath}${fileName}'${attrs}></script>`;
                })
                .join('\n');

            const links = (files.css || [])
                .map(({ fileName }) => {
                    const attrs = makeHtmlAttributes(attributes.link);
                    return `<link rel='stylesheet' href='${publicPath}${fileName}'${attrs}>`;
                })
                .join('\n');

            const metas = meta
                .map((input) => {
                    const attrs = makeHtmlAttributes(input);
                    return `\t<meta${attrs}>`;
                })
                .join('\n');


            const source = homePageTemplate(metas, title, links, scripts);

            const htmlFile = {
                type: 'asset',
                source,
                name: 'Rollup HTML Asset',
                fileName
            };
            this.emitFile(htmlFile);
        }
    };
}