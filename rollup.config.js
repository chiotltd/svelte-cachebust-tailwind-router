import svelte from 'rollup-plugin-svelte';
import commonjs from '@rollup/plugin-commonjs';
import resolve from '@rollup/plugin-node-resolve';
import livereload from 'rollup-plugin-livereload';
import { terser } from 'rollup-plugin-terser';
import sveltePreprocess from 'svelte-preprocess';
import typescript from '@rollup/plugin-typescript';
import styles from 'rollup-plugin-styles';
import {indexTemplate} from './build-util/index-template'
import {generateDist} from './build-util/generate-dist'

const production = !process.env.ROLLUP_WATCH;
const build_dir = "public";
const prod_dir = "dist";

function serve() {
	let server;

	function toExit() {
		if (server) server.kill(0);
	}

	return {
		writeBundle() {
			if (server) return;
			server = require('child_process').spawn('npm', ['run', 'start', '--', '--dev'], {
				stdio: ['ignore', 'inherit', 'inherit'],
				shell: true
			});

			process.on('SIGTERM', toExit);
			process.on('exit', toExit);
		}
	};
}

export default {
	input: 'src/main.ts',
	output: {
		sourcemap: true,
		format: 'iife',
		name: 'app',
		dir: 'public',
		entryFileNames: `build/[name]${production ? '.[hash]' : ''}.js`,
		chunkFileNames: `build/[name].chunk${production ? '.[hash]' : ''}.js`,
		assetFileNames: `build/[name]${production ? '.[hash]' : ''}.[ext]`,
	},
	plugins: [
		svelte({
			preprocess: sveltePreprocess({
				// https://github.com/kaisermann/svelte-preprocess/#user-content-options
				sourceMap: !production,
				postcss: {
					plugins: [require("tailwindcss"), require("autoprefixer"), require("postcss-nesting")]
				}
			}),
			compilerOptions: {
				// enable run-time checks when not in production
				dev: !production
			}
		}),
		// rollup-plugin-styles - currently only used to extract CSS for better performance
		styles({
			mode: "extract",
			minimize: production,
		}),

		// If you have external dependencies installed from
		// npm, you'll most likely need these plugins. In
		// some cases you'll need additional configuration -
		// consult the documentation for details:
		// https://github.com/rollup/plugins/tree/master/packages/commonjs
		resolve({
			browser: true,
			dedupe: ['svelte']
		}),
		commonjs(),
		typescript({
			sourceMap: true,
			inlineSources: !production
		}),

		// In dev mode, call `npm run start` once
		// the bundle has been generated
		!production && serve(),

		// Watch the `public` directory and refresh the
		// browser on changes when not in production
		!production && livereload('public'),

		// If we're building for production (npm run build
		// instead of npm run dev), minify
		production && terser(),

		// For production builds index.html needs to be rewritten with filenames containing th hashes
		// note: this only covers the bundle files (global.css is fixed later)
		production && indexTemplate(),

		// Production builds will be moved to the dist folder and a hash added to global.css
		production && generateDist(build_dir, prod_dir)
	],
	watch: {
		clearScreen: false
	}
};
